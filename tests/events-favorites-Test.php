<?php

class EventsFavoritesTest extends \WP_Mock\Tools\TestCase {

  public static function setUpBeforeClass(): void {
    // test adding filters and actions here
    \WP_Mock::expectActionAdded( 'tribe_events_event_schedule_details_inner', 'add_favorite_button', 100 );
    \WP_Mock::expectActionAdded( 'admin_menu', 'setup_admin_menu' );

    include( __DIR__ . '/../events-favorites.php' );
  }

  public function setUp(): void {
    \WP_Mock::setUp();
  }

  public function tearDown(): void {
    \WP_Mock::tearDown();
  }

  public function testInit() {
    $expected = "<h1>Events Favorite Configuration Page</h1>" . "<button>Click me to do nothing</button>";
    $this->expectOutputString($expected);
    init();
  }

 /*
  //TODO: Figure out why it can't see the setup admin menu function
  public function testSetupAdminPage() {
    setup_admin_menu();
  }
 */

  public function testAddFavoriteButton() {
    $value = "Beans";
    $expected = "Beans" . "<button>Have some beans </button>";
    $this->assertEquals(add_favorite_button($value), $expected);
  }
}
