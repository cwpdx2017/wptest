<?php
/**
 * Plugin Name:     "Add to Favorite / Remove from Favorite" Button
 * Description:     An extension that adds a button near the event title
 * Version:         1.0.0
 * Extension Class: Tribe__Extension__Event_Favorite_Button
 * Author:          pdxcap WC
 * Author URI:      http://so
 * License:         GPLv2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 */

if ( !defined( 'ABSPATH' ) ) {
    die('-1');
}

// Do not load unless Tribe Common is fully loaded.
if (! class_exists( 'Tribe__Extension') ) {
    return;
}


class Tribe__Extension__Event_Favorite_Button extends Tribe__Extension {
    public function construct() {
        #$this->add_required_plugin( 'Tribe__Events__Main', '4.3' );
    }

    public function init() {
        // For single event page
        
        add_filter( 'tribe_events_template_single-event.php', 
            function ( $file ) {
                return plugin_dir_path( __FILE__ ) . 'src/templates/single-event.php';
            } 
        );
        add_filter( 'tribe_get_template_part_path_list/single-event.php', 
            function ( $file ) {
                return plugin_dir_path( __FILE__ ) . 'src/templates/list/single-event.php';
            }
        );
        
        add_filter( 'tribe_get_template_part_path_day/single-event.php', 
            function ( $file ) {
                return plugin_dir_path( __FILE__ ) . 'src/templates/day/single-event.php';
            }
        );
    }
}

require_once plugin_dir_path( __FILE__ ) . 'src/base.php';
require_once plugin_dir_path( __FILE__ ) . 'src/uninstall.php';

if (class_exists ('Base\\PluginBase')) {
    $pluginbase = new Base\PluginBase;
    $pluginbase->register();
}

register_activation_hook( __FILE__, array($pluginbase, 'activate') );
register_activation_hook( __FILE__, array($pluginbase, 'deactivate') );
