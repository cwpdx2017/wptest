<?php
namespace Base;

class PluginBase {

    public $plugin_name;

    function __construct() {
        $this->plugin_name = plugin_dir_url(__FILE__);
       // print "                <pre>"; print_r($this->plugin_name . 'assets/event_favorite.css'); print "</pre>";
    }

    function register() {
        $this->register_wp();
        $this->register_admin();
    }
    function register_wp() {
        add_action ('wp_enqueue_scripts', array( $this, 'enqueue') );
        //print "<pre>"; print_r(plugin_dir_path(dirname( __FILE__, 2)). 'assets/event_favorite.css'); print "</pre>";
    }
    function register_admin() {

    }

    function activate() {
        flush_rewrite_rules();
    }

    function deactivate() {
        flush_rewrite_rules();
    }

    function enqueue() {
    wp_enqueue_style('event_favorite_style', $this->plugin_name . 'assets/event_favorite.css');
        //wp_enqueue_style( 'event_favorite_style');
        #wp_enqueue_script( 'checklogin', $this->plugin_name . 'assets/checklogin.js');
    }

}

